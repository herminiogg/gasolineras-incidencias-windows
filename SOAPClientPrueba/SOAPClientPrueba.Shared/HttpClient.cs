﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace SOAPClientPrueba
{
    class HttpClient
    {
        public T Get<T>(string url)
        {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            HttpWebResponse response = request.GetResponseAsync().Result as HttpWebResponse;
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(String.Format(
                "Server error (HTTP {0}: {1}).",
                response.StatusCode,
                response.StatusDescription));
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(T));
            object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
            return (T) objResponse;
        }
    }
}
