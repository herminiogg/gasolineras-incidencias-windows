﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace SOAPClientPrueba
{
    class MyGeolocator
    {
        public async Task<double[]> GetCoordinates()
        {
            Geolocator geo = new Geolocator();
            geo.DesiredAccuracyInMeters = 1000;
            Geoposition pos = await geo.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
            return new double[] {
                pos.Coordinate.Point.Position.Latitude,
                pos.Coordinate.Point.Position.Longitude
            };

        }
    }
}
