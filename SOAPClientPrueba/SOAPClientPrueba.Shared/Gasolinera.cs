﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SOAPClientPrueba
{
    [DataContract]
    class Gasolinera
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }
        [DataMember(Name = "provincia_id")]
        public long ProvinciaId { get; set; }
        [DataMember(Name = "municipio_id")]
        public long MunicipioId { get; set; }
        [DataMember(Name = "localidad_id")]
        public long LocalidadId { get; set; }
        [DataMember(Name = "codigo_postal")]
        public int CodigoPostal { get; set; }
        [DataMember(Name = "direccion")]
        public string Direccion { get; set; }
        [DataMember(Name = "longitud")]
        public double Longitud { get; set; }
        [DataMember(Name = "latitud")]
        public double Latitud { get; set; }
        [DataMember(Name = "rotulo")]
        public string Rotulo { get; set; }
        [DataMember(Name = "horario")]
        public string Horario { get; set; }
        [DataMember(Name = "precios")]
        public Precios Precios { get; set; }
        

        public override string ToString()
        {
            return this.Rotulo + " - " + this.Direccion + "\n" + this.Horario;
        }
    }

    [DataContract]
    class Precios
    {
        [DataMember(Name = "gasolina_98")]
        public string Gasolina98 { get; set; }
        [DataMember(Name = "gasolina_95")]
        public string Gasolina95 { get; set; }
        [DataMember(Name = "gasoleo_A")]
        public string GasoleoA { get; set; }
        [DataMember(Name = "nuevo_gasoleo_A")]
        public string NuevoGasoleoA { get; set; }
    }
}
