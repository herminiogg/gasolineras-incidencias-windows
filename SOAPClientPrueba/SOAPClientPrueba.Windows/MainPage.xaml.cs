﻿using SOAPClientPrueba.TraficoWebService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SOAPClientPrueba
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(WebServiceResult), radio.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RestResult), radio.Text);
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void radio_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string tipo = (tipoIncidencia.SelectedItem as ComboBoxItem).Content.ToString();
            string descripcionText = descripcion.Text;
            double[] coordinates = await new MyGeolocator().GetCoordinates();
            TraficoWebServiceClient traficoClient = new TraficoWebServiceClient();
            
            if (tipo.Equals("Alerta"))
            {
                await traficoClient.addAlertaAsync(coordinates[0], coordinates[1], descripcionText);
            }
            else if (tipo.Equals("Control"))
            {
                await traficoClient.addControlAsync(coordinates[0], coordinates[1], descripcionText);
            }
            else if (tipo.Equals("Radar"))
            {
                await traficoClient.addRadarAsync(coordinates[0], coordinates[1], descripcionText);
            }
            else
            {
                throw new Exception("Tipo de incidencia no soportado");
            }
            this.SendNotification();
        }

        private void SendNotification()
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastImageAndText01;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
            toastTextElements[0].AppendChild(toastXml.CreateTextNode("Incidencia añadida. Gracias por su colaboración"));
            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((XmlElement)toastNode).SetAttribute("launch", "{\"type\":\"toast\",\"param1\":\"12345\",\"param2\":\"67890\"}");
            ToastNotification toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void addIncidencia()
        {

        }

    }
}
